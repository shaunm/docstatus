# docstatus

This is docstatus, a super simple documentation status tracker.
It outputs pages with tables of statuses of pages for a bunch of docs.
To run it, just run `docstatus`.
It will output to the __build__ directory.
Copy docstatus.css into the __build__ directory and view the HTML files.

docstatus is a Python 3 script.
Aside from the standard library, it requires lxml and mako.
You probably already have both installed.

To add repositories, edit the space-separated REPOS file.
The first column is the git URL to clone.
The second column is the directory in the git repo containing index.page.
You can optionally add an identifier in the third column.
Normally the identifier automatically comes from the git URL,
but that doesn't work when you have a repository with multiple documents.

To add branches, edit the space-separated VERSIONS file.
The first column is the git branch to use.
The second column is the version tag to look for in pages in that branch.
Currently, docstatus looks for the version tag in the version, docversion,
and pkgversion attributes.
See [MEP-0006](http://projectmallard.org/mep/mep0006) for information on
plans to change how that all works.

docstatus assumes all its projects have the same branch names.
If a project is missing a branch, it checks that version against master
and puts a warning on the output page.
